package projectmanagement.bubblechart.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import projectmanagement.bubblechart.model.Dish;
import projectmanagement.bubblechart.model.Order;
import projectmanagement.bubblechart.repository.OrderRepository;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    /*
        @Mock - mockito на лету создаст реализацию интерфейса, причем такую какую мы скажем

        Вот пример. У нас есть orderRepository который как то сохраняет в  базу объекты типа Order. Как он это делает
        нам срать. поэтому в целях тестирования мы можем сказать, что при сохранении любого Order возвращается true

        Mockito.when(orderRepository.save(any(Order.class)))
                .thenReturn(true);

        Или если мы хотим затестить поведение, когда база не хочет сохранять чтото, по какой то причине

         Mockito.when(orderRepository.save(any(Order.class)))
                .thenReturn(false);
     */

    // какой то интерфес который умеет что то узнать у внешней системы (про наличие ингредиентов например)
    @Mock
    private IngredientService ingredientService;

    // какой то интерфейс, который умеет что то сохранить в базу (сохраняет заказы)
    @Mock
    private OrderRepository orderRepository;

    //Какой то интрерфейс, который что то делает с заказами его мы и тестируем (хрень, что будет дергаться при запросах)
    private OrderService orderService;

    private Order order;

    @BeforeEach
    void init() {
        orderService = new OrderServiceImpl(orderRepository, ingredientService);
        order = new Order();
        order.setDishes(Arrays.asList(new Dish(), new Dish(), new Dish()));
    }

    //например он умеет добавить блюдо в заказ
    @Test
    void createOrder() {
        Mockito.when(ingredientService.checkDish(any(Dish.class)))
                .thenReturn(true);

        Mockito.when(orderRepository.save(any(Order.class)))
                .thenReturn(true);

        assertTrue(orderService.addDishToOrder(order));
    }

    //но когда нет ингредиентов оно должно вернуть false
    @Test
    void createOrderWithouIngredients() {
        Mockito.when(ingredientService.checkDish(any(Dish.class)))
                .thenReturn(false);

        assertFalse(orderService.addDishToOrder(order));
    }

    //иногда новый заказ не может быть добавлен потому что, что то пошло не так в базе и она вернула false
    @Test
    void createOrderDataBaseIsDead() {
        Mockito.when(ingredientService.checkDish(any(Dish.class)))
                .thenReturn(true);

        Mockito.when(orderRepository.save(any(Order.class)))
                .thenReturn(false);

        assertFalse(orderService.addDishToOrder(order));
    }
}
