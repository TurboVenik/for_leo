package projectmanagement.bubblechart.service;

import projectmanagement.bubblechart.model.Order;

public interface OrderService {

    boolean addDishToOrder(Order order);
}
