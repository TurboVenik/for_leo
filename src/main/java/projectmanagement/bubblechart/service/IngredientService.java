package projectmanagement.bubblechart.service;

import projectmanagement.bubblechart.model.Dish;

public interface IngredientService {
    boolean checkDish(Dish dish);
}
