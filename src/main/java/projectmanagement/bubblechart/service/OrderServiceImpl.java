package projectmanagement.bubblechart.service;

import projectmanagement.bubblechart.model.Dish;
import projectmanagement.bubblechart.model.Order;
import projectmanagement.bubblechart.repository.OrderRepository;

public class OrderServiceImpl implements OrderService {

    private IngredientService ingredientService;

    private OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository, IngredientService ingredientService) {
        this.orderRepository = orderRepository;
        this.ingredientService = ingredientService;
    }

    public boolean addDishToOrder(Order order) {
        //перед тем как добавить надо узнать для всех ли блюд есть ингредиенты
        //знает об этом внешняя система склада
        for (Dish dish : order.getDishes()) {
            if (!ingredientService.checkDish(dish)) {
                return false;
            }
        }

        //все ингредиенты есть, значит можно добавить заказ в базу
        return orderRepository.save(order);
    }
}
