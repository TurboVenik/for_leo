package projectmanagement.bubblechart.repository;

import projectmanagement.bubblechart.model.Order;

public interface OrderRepository {

    boolean save(Order order);
}
