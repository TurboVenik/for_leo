package projectmanagement.bubblechart.model;

import lombok.Data;

import java.util.List;

@Data
public class Order {

    private int id;
    private List<Dish> dishes;
}
