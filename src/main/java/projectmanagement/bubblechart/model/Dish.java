package projectmanagement.bubblechart.model;

import lombok.Data;

@Data
public class Dish {

    private String name;
}
