package projectmanagement.bubblechart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AttributeTest {

    @BeforeEach
    void initAll() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
    }

    @Test
    public void getAttributeName() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        assertEquals("attribute_name", attribute1.getAttributeName());
    }

    @Test
    public void setAttributeName() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        attribute1.setAttributeName("attribute_name_1");
        assertEquals("attribute_name_1", attribute1.getAttributeName());
    }

    @Test
    public void getAttributeType() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        assertEquals(type1, attribute1.getAttributeType());
    }

    @Test
    public void setAttributeType() {
        Type type1 = new Type ("typeName");
        Type type2 = new Type ("typeName2");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        attribute1.setAttributeType(type2);
        assertEquals(type2, attribute1.getAttributeType());
    }

    @Test
    public void getAttributeValue() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        assertEquals(10.0, attribute1.getAttributeValue());
    }

    @Test
    public void setAttributeValue() {
        Type type1 = new Type ("typeName");
        Factor factor = new Factor("factorName", type1, 11);
        Attribute attribute1 = new Attribute("attribute_name", type1, 10.0, 1, factor);
        attribute1.setAttributeValue(11.1);
        assertEquals(11.1, attribute1.getAttributeValue());
    }
}
