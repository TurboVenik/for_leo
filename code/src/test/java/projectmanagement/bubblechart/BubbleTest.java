package projectmanagement.bubblechart;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BubbleTest {


    @Test
    public void getBubbleNumber() {
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        assertEquals(1, bubble.getBubbleNumber());
    }

    @Test
    public void setBubbleNumber() {
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.setBubbleNumber(2);
        assertEquals(2, bubble.getBubbleNumber());
    }

    @Test
    public void getBubbleName() {
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        assertEquals("bubbleName", bubble.getBubbleName());
    }

    @Test
    public void setBubbleName() {
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.setBubbleName("bubbleName_2");
        assertEquals("bubbleName_2", bubble.getBubbleName());
    }

    @Test
    public void changeAttribute() {
        Type type1 = new Type ("typeName");
        Type type2 = new Type ("typeName2");
        Factor factor = new Factor("factorName", type1);
        Factor factor2 = new Factor("factorName", type2);
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.addAttribute("attributeName", type1, 111, 2, factor);
        assertEquals(0, bubble.changeAttribute("attributeName", factor2));
        assertEquals(1, bubble.changeAttribute("bad_name", factor2));
    }

    @Test
    public void addAttribute() {
        Type type1 = new Type ("typeName");
        Type type2 = new Type ("typeName2");
        Factor factor = new Factor("factorName", type1);
        Factor factor2 = new Factor("factorName", type2);
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.addAttribute("attributeName", type1, 111, 2, factor);
        assertEquals(1,bubble.addAttribute("attributeName", type1, 111, 2, factor));
        assertEquals(0,bubble.addAttribute("attributeName2", type1, 111, 2, factor2));
    }

    @Test
    public void delAttribute() {
        Type type1 = new Type ("typeName");
        Type type2 = new Type ("typeName2");
        Factor factor = new Factor("factorName", type1);
        Factor factor2 = new Factor("factorName", type2);
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.addAttribute("attributeName", type1, 111, 2, factor);
        assertEquals(0, bubble.delAttribute(factor));
        assertEquals(1, bubble.delAttribute(factor2));
    }

    @Test
    public void delAttribute1() {
        Type type1 = new Type ("typeName");
        Type type2 = new Type ("typeName2");
        Factor factor = new Factor("factorName", type1);
        Factor factor2 = new Factor("factorName", type2);
        Project project = new Project ("projectName");
        Bubble bubble = new Bubble(1, "bubbleName", project);
        bubble.addAttribute("attributeName", type1, 111, 2, factor);
        assertEquals(0, bubble.delAttribute("attributeName"));
        assertEquals(1, bubble.delAttribute("bad_attributeName"));
    }
}
