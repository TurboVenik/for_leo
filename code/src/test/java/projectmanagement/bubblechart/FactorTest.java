package projectmanagement.bubblechart;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FactorTest {


    @Test
    public void getFactorName() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1);
        assertEquals("factorname_1", f1.getFactorName());
    }

    @Test
    public void setFactorName() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1);
        f1.setFactorName("factorname_2");
        assertEquals("factorname_2", f1.getFactorName());

    }

    @Test
    public void getFactorType() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1);
        assertEquals(type1, f1.getFactorType());

    }

    @Test
    public void setFactorType() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1);
        f1.setFactorType(type1);
        assertEquals(type1, f1.getFactorType());

    }

    @Test
    public void getFactorValue() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1, 11);
        assertEquals(11, f1.getFactorValue());

    }

    @Test
    public void setFactorValue() {
        Type type1 = new Type ("typeName");
        Factor f1 = new Factor("factorname_1", type1, 10);
        f1.setFactorValue(12);
        assertEquals(12, f1.getFactorValue());

    }
}
