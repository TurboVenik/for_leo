package projectmanagement.bubblechart;

public class Attribute {
    private String attributeName;
    private Type attributeType;
    private double attributeValue;
    private  int attributeId;
    Factor factor;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Type getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(Type attributeType) {
        this.attributeType = attributeType;
    }

    public double getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(double attributeValue) {
        this.attributeValue = attributeValue;
    }

    public Attribute(String attributeName, Type attributeType, double attributeValue, int attributeId, Factor factor) {
        this.attributeName = attributeName;
        this.attributeType = attributeType;
        this.attributeValue = attributeValue;
        this.attributeId = attributeId;
        this.factor = factor;
    }
}