package projectmanagement.bubblechart;

import java.util.ArrayList;
import java.util.List;

public class Bubble {

    List<Attribute> attributes = new ArrayList<Attribute>();
    private int bubbleNumber;
    private String BubbleName;
    Project project;

    public  int getBubbleNumber() {
        return bubbleNumber;
    }

    public  void setBubbleNumber(int bubbleNumber) {
        this.bubbleNumber = bubbleNumber;
    }

    public String getBubbleName() {
        return BubbleName;
    }

    public void setBubbleName(String bubbleName) {
        BubbleName = bubbleName;
    }


    public Bubble(int bubbleNumber, String bubbleName, Project project) {
        List<Attribute> attributes = new ArrayList<Attribute>();
        this.bubbleNumber = bubbleNumber;
        this.BubbleName = bubbleName;
        this.project = project;
    }

    public int changeAttribute(String attributeName, Factor factor){
        for (Attribute a : attributes) {
            if (a.getAttributeName().equals(attributeName)) {
                a.factor = factor;
                return 0;
            }
        }
        return 1;
    }

    public int addAttribute(String attributeName, Type attributeType, float attributeValue, int attributeId, Factor factor) {
        for (Attribute a : attributes) {
            if (a.factor.equals(factor)) {
                return 1;
            }
        }
        Attribute a1 = new Attribute(attributeName, attributeType, attributeValue, attributeId, factor);
        attributes.add(a1);
        return 0;
    }

    public int delAttribute(Factor factor) {
        for (Attribute a : attributes) {
            if (a.factor.equals(factor)) {
                attributes.remove(a);
                return 0;
            }
        }
        return 1;
    }

    public int delAttribute(String attributeName) {
        for (Attribute a : attributes) {
            if (a.getAttributeName().equals(attributeName)) {
                attributes.remove(a);
                return 0;
            }
        }
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Bubble)) {
            return false;
        }
        Bubble bb = (Bubble) obj;
        if (bb.getBubbleName().equals(this.getBubbleName()) && bb.getBubbleNumber() == this.getBubbleNumber())
            return true;
        else
            return false;
        }
}
