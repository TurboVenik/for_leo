package service;

import projectmanagement.bubblechart.BubbleDiagram;

public interface UserService {
    void downloadData(ConnectorService connectorService);

    BubbleDiagram makeDiagram();
}
