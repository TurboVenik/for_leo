package service;

import users.User;

public interface AdminService {
    User findByName(String userName);

    User addUser(User userBeforeSave);

    User editUser(User userAfterSave);

}
