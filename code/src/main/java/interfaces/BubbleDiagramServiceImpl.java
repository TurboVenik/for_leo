package interfaces;

import interfaces.dto.DiagramDto;
import projectmanagement.bubblechart.BubbleDiagram;
import repository.BubbleDiagramRepository;

import java.util.List;

public class BubbleDiagramServiceImpl implements BubbleDiagramService {

    private BubbleDiagramRepository bubbleDiagramRepository;

    public BubbleDiagramServiceImpl(BubbleDiagramRepository bubbleDiagramRepository) {
        this.bubbleDiagramRepository = bubbleDiagramRepository;
    }

    @Override
    public List<DiagramDto> findDiagramList() {
        return bubbleDiagramRepository.findDiagramList();
    }

    @Override
    public BubbleDiagram findById(int id) {
        return bubbleDiagramRepository.findById(id);
    }

    @Override
    public Integer deleteById(int id) {
        return bubbleDiagramRepository.deleteById(id);
    }

    @Override
    public Integer addBubbleDiagram(BubbleDiagram bubbleDiagram) {
        return bubbleDiagramRepository.addBubbleDiagram(bubbleDiagram);
    }

    @Override
    public Integer editBubbleDiagram(BubbleDiagram bubbleDiagram) {
        return bubbleDiagramRepository.editBubbleDiagram(bubbleDiagram);
    }
}
