package repository;

import users.User;

import java.util.List;

public interface UserRepository {

    User findByName(String userName);

    User addUser(User user);

    User editUser(User user);

    List<User> findAll();
}
