package repository;

import interfaces.dto.DiagramDto;
import projectmanagement.bubblechart.BubbleDiagram;

import java.util.List;

public interface BubbleDiagramRepository {

    List<DiagramDto> findDiagramList();

    Integer deleteById(int id);

    BubbleDiagram findById(int id);

    Integer addBubbleDiagram(BubbleDiagram bubbleDiagram);

    Integer editBubbleDiagram(BubbleDiagram bubbleDiagram);
}
